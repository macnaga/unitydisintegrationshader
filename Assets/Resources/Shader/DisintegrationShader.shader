﻿Shader "Custom/DisintegrationShader"
{
 Properties
 {
     _MainTex ("Source", 2D) = "white" {}
     _StrengthX("StrengthX",Range(-500,500)) = 1
     _StrengthY("StrengthY",Range(0,10)) = 1
     _BeginY("BeginY",Range(0,1))=0.9
     _EndY("EndY",Range(0,1))=1
 }

 SubShader
 {
     Blend SrcAlpha OneMinusSrcAlpha

     ZTest Always
     Cull Off
     ZWrite Off
     Fog { Mode Off }

     Pass
     {
         CGPROGRAM
         #pragma vertex vert
         #pragma fragment frag

         #include "UnityCG.cginc"

         struct appdata
         {
             float4 vertex : POSITION;
             fixed4 color    : COLOR;
             float2 uv : TEXCOORD0;
         };

         struct v2f
         {
             float2 uv : TEXCOORD0;
             UNITY_FOG_COORDS(1)
             float4 vertex : SV_POSITION;
             fixed4 color    : COLOR;
         };

         sampler2D _MainTex;
         float4 _MainTex_ST;
         float _StrengthX;
         float _StrengthY;
         float _BeginY;
         float _EndY;

         float rand(float2 co)
         {
             return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
         }

         v2f vert (appdata v)
         {
             v2f o;
             o.vertex = UnityObjectToClipPos(v.vertex);
             o.uv = TRANSFORM_TEX(v.uv, _MainTex);
             o.color = v.color;
             return o;
         }
         
         fixed4 frag (v2f i) : SV_Target
         {
             float2 vCenter = float2(_ScreenParams.x/2.f,_ScreenParams.y/2.f);
             float2 vScreenSize = float2( _ScreenParams.x , _ScreenParams.y );
             float2 pos = ( i.uv * vScreenSize ) - vCenter;
             float x = pos.x;
             float y = pos.y;
             float alpha = 1.0f;
             float uvY = (1-(i.uv.y));
             float r = rand(i.uv);
             if( _BeginY <= uvY && uvY <= _EndY )
             {
                 x += r *_StrengthX;
                 y -= r *_StrengthY;
                 alpha *= 100.0f/(_StrengthY+50.0f);
             }

             float2 retPos =  (float2( x, y ) + vCenter  ) / vScreenSize ;
             fixed4 c = tex2D(_MainTex, retPos ) * i.color;
             c.rgb *= c.a * alpha;
             c.a *= alpha;
             return c;
         }
         ENDCG
     }
 }
}
