﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour
{
    [SerializeField]
    Material mat;

    [SerializeField]
    float tick = 0.001f;

    float t = 1f;

    void Update ()
    {
        t -= tick;
        mat.SetFloat ("_AnimTimeY", t);
    }
}